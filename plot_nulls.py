import numpy as np
from matplotlib import pyplot as plt

# load in data from files
null_coeffs = np.loadtxt('mfis/null_coeffs_no_abs.txt')
null_median_errors = np.loadtxt('mfis/null_median_errors.txt')
null_sq_errors = np.loadtxt('mfis/null_sq_errors.txt')

# extract sleep and maia coefficient data
null_sleep_coeffs = []
for row in null_coeffs:
    null_sleep_coeffs.append(row[14])

x_maia38 = []
for row in null_coeffs:
    x_maia38.append(row[2])

# compute p-value
num_better_scores = np.count_nonzero(null_sleep_coeffs[1:] >= null_sleep_coeffs[0])
print('Number of better permuted scores:', num_better_scores)
pval = (num_better_scores + 1) / (999 + 1)
print('p-value:', pval)

# plot null distribution
fig, ax = plt.subplots()
ax.axvline(null_sleep_coeffs[0], ls='--', color='r')
plt.hist(null_sleep_coeffs[1:], bins=25)

# plot p-value
score_label = (f"Score: {null_sleep_coeffs[0]:.2f}\n"
               f"(p: {pval:.3f})")
ax.text(null_sleep_coeffs[0] + 0.1, 600, score_label, fontsize=10)
ax.set_xlabel("Sleep")
ax.set_ylabel("Number of tests")
plt.show()