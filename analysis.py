import pandas as pd
import numpy as np
from sklearn.linear_model import ElasticNet
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error
from sklearn.metrics import median_absolute_error
from sklearn.preprocessing import StandardScaler

# load predictors/features
X = pd.read_csv('mfis/mfis_X.csv', header=None, sep='\t')

# load outcomes
y = pd.read_csv('mfis/mfis_y.csv', header=None)

# instantiate model and define hyperparameter search space
model = ElasticNet(random_state=1)
parameters = {
    'l1_ratio': np.geomspace(0.01, 1, 10),  # alpha in glmnet
    'alpha': np.geomspace(0.01, 100, 10)  # lambda in glmnet
}

null_sq_errors = []
null_coeffs = []
null_median_errors = []
for i in range(1000):
    print('permutation:', i)
    # first iteration is the "real"/unpermuted run whereas the following 999 are shuffled/permuted
    if i > 0:
        y[0] = np.random.permutation(y[0])
    coefficients = np.zeros(15)
    sq_errors = []
    median_errors = []
    # cross-validation outer loop
    cv_outer = KFold(n_splits=10, shuffle=True, random_state=1)
    for train_ix, test_ix in cv_outer.split(X):
        # split data
        X_train, X_test = X.iloc[train_ix, :], X.iloc[test_ix, :]
        y_train, y_test = y.iloc[train_ix], y.iloc[test_ix]

        # z-scale train and test predictors based on train predictors
        z_scaler  = StandardScaler().fit(X_train)
        X_train = z_scaler.transform(X_train)
        X_test  = z_scaler.transform(X_test)

        # mean center train and test outcomes based on train outcomes
        center_scaler = StandardScaler(with_std=False).fit(y_train)
        y_train = center_scaler.transform(y_train)
        y_test = center_scaler.transform(y_test)

        # configure the inner loop cross-validation procedure
        cv_inner = KFold(n_splits=10, shuffle=True, random_state=1)
        # define search
        search = GridSearchCV(model, parameters, scoring='neg_mean_squared_error', n_jobs=1, cv=cv_inner, refit=True, verbose=0)
        # execute search
        result = search.fit(X_train, y_train)
        # get the best performing model fit on the whole training set
        best_model = result.best_estimator_
        # evaluate model on the test set
        yhat = best_model.predict(X_test)
        # evaluate the model
        sq_error = mean_squared_error(y_test, yhat)
        median_error = median_absolute_error(y_test, yhat)
        # store the result
        sq_errors.append(sq_error)
        median_errors.append(median_error)
        # report progress
        print('>sq. error=%.3f, est=%.3f, cfg=%s' % (sq_error, result.best_score_, result.best_params_))
        # keep track of coefficients
        coefficients += best_model.coef_

    # avg coefficients across the 10 folds
    coefficients /= 10

    # keep track of metrics
    null_sq_errors.append(np.mean(sq_errors))
    null_median_errors.append(np.mean(median_errors))
    null_coeffs.append(coefficients)

    # plot coefficient values
    # feature_importance = pd.Series(index = [i for i in range(15)], data = coefficients)
    # feature_importance.sort_values().tail(30).plot(kind = 'bar', figsize = (18,6))
    # plt.show()

    # summarize the estimated performance of the model
    print('sq. error: %.3f (%.3f)' % (np.mean(sq_errors), np.std(sq_errors)))

# save all permutations to file for later plotting
np.savetxt('mfis/null_sq_errors.txt', null_sq_errors)
np.savetxt('mfis/null_coeffs_no_abs.txt', null_coeffs)
np.savetxt('mfis/null_median_errors.txt', null_median_errors)