# Rouault et al. Fatigue Elastic Net Analysis

## Description

We performed a multivariate linear regression analysis with nested cross-validation and elastic net regularisation (Zou and Hastie, 2005). This analysis included all autonomic, interoceptive and task-based measurements and aimed at identifying the most meaningful predictor(s) of fatigue scores without a priori hypotheses or preselection of regressors. Elastic net regularisation introduces two penalty terms to the ordinary least squares objective function that combine properties of lasso (L1 penalty) and ridge regression (L2 penalty), allowing selection of a solution with particular properties. In particular, for problems with few data points compared to the number of regressors, this avoids overfitting, thus enhancing the accuracy of the predictors, through automatic variable selection and shrinkage of large regression coefficients. 

In our application of elastic net regression implemented in scikit-learn, we z-score the predictors (regressors) and mean-centre the outcome variable (fatigue scores). all measurement values and the dependent variable (fatigue scores) were first scaled using a z-score transform  . As in Rouault et al. (2018), we implemented ten-fold cross-validation (CV), with nested cross-validation for tuning the hyperparameters. The data were randomly split into 10 sets (folds). A model was then generated based on 9 training folds, and applied to the remaining independent validation set. Each fold served as the validation set once, resulting in 10 different models and predictions. Nested cross-validation involved subdividing the 9 training sets (i.e., 90% of the sample) into a further 10 folds (“inner” folds). Within these 10 inner folds, 9 were utilized for training a model over a range of 1050 alpha (0.01–1) and 50 10 lambda (0.001–100) values, where alpha is the complexity parameter and lambda is the regularisation coefficient. This resulted in a model fit for the inner test set for each possible combination of alpha and lambda. The best fit over all 10 inner folds for each combination of alpha and lambda was then used to determine the optimal parameters for each outer fold. We tested the significance of regression coefficients using permutation tests with 1,000 permutations.

## Code
After downloading the repository and navigating to its directory in a terminal, use `python analysis.py` to run the elastic net analysis (including permutation test). After it is finished, run `python plot_nulls.py` to generate the plots.

## References
Rouault, M., Seow, T., Gillan, C.M., Fleming, S.M., 2018. Psychiatric Symptom Dimensions Are Associated With Dissociable Shifts in Metacognition but Not Task Performance. Biological Psychiatry.

Zou, H., Hastie, T., 2005. Regularization and variable selection via the elastic net. Journal of the Royal Statistical Society: Series B (Statistical Methodology) 67, 301–320.
